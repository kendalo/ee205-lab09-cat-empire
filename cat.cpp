///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 09a - Cat Empire!
///
/// @file cat.cpp
/// @version 1.0
///
/// Exports data about all cats
///
/// @author @Kendal Oya <kendalo@hawaii.edu>
/// @brief  Lab 09a - Cat Empire! - EE 205 - Spr 2021
/// @date   @May 2 2021
///////////////////////////////////////////////////////////////////////////////

#include <cassert>
#include <random>
#include <queue>

#define CAT_NAMES_FILE "names.txt"

#include "cat.hpp"

using namespace std;

Cat::Cat( const std::string newName ) {
	setName( newName );
}


void Cat::setName( const std::string newName ) {
	assert( newName.length() > 0 );

	name = newName;
}


std::vector<std::string> Cat::names;


void Cat::initNames() {
	names.clear();
	cout << "Loading... ";

	ifstream file( CAT_NAMES_FILE );
	string line;
	while (getline(file, line)) names.push_back(line);

	cout << to_string( names.size() ) << " names." << endl;
}


Cat* Cat::makeCat() {
	// Get a random cat from names
	auto nameIndex = rand() % names.size();
	auto name = names[nameIndex];

	// Remove the cat (by index) from names
	erase( names, names[nameIndex] );

	return new Cat( name );
}


void CatEmpire::catFamilyTree() const {
	if( topCat == nullptr ) {
		cout << "No cats!" << endl;
		return;
	}

	dfsInorderReverse( topCat, 1 );
}


void CatEmpire::addCat( Cat* newCat ){
   if( topCat == nullptr ){
      topCat = newCat;
      return; 
   }
   addCat( topCat, newCat );
}

void CatEmpire::addCat( Cat* atCat, Cat* newCat ){
   if( atCat->name > newCat->name ){
      if( atCat-> left == nullptr ){
         atCat->left = newCat;
      }else{
         addCat( atCat->left, newCat );
      }   
   }

   if( atCat->name < newCat->name ){
      if( atCat-> right == nullptr ){
         atCat->right = newCat;
      }else{
         addCat( atCat->right, newCat );
      }
   }
}

void CatEmpire::catList() const {
	if( topCat == nullptr ) {
		cout << "No cats!" << endl;
		return;
	}

	dfsInorder( topCat );
}


void CatEmpire::catBegat() const {
	if( topCat == nullptr ) {
		cout << "No cats!" << endl;
		return;
	}

	dfsPreorder( topCat );
}

void CatEmpire::dfsInorder( Cat* atCat ) const {
      if( atCat == nullptr ) return;
      dfsInorder( atCat->left );
      cout << atCat->name << endl;
      dfsInorder( atCat->right );
}

void CatEmpire::dfsInorderReverse( Cat* atCat, int depth ) const {
      if( atCat == nullptr ) return;
      dfsInorderReverse( atCat-> right, depth+1 );

      cout << string( 6*(depth-1), ' ' ) << atCat->name;
      if( atCat -> left == nullptr && atCat-> right == nullptr )
         cout << endl;
      //left and right
      else if( atCat-> left != nullptr && atCat-> right != nullptr )
         cout << "<" << endl;
      //left
      else if( atCat-> left != nullptr && atCat-> right == nullptr )
         cout << "\\" << endl;
      //right
      else if( atCat-> left == nullptr && atCat-> right != nullptr )
         cout << "/" << endl;
      dfsInorderReverse( atCat-> left, depth+1 );
      return;
}


void CatEmpire::dfsPreorder( Cat* atCat ) const {
   if( atCat == nullptr ) return; 
   if( atCat-> right != nullptr && atCat->left != nullptr ){
      cout << atCat->name << " begat " << atCat->left->name << " and " << atCat->right->name << endl;
   }
   else if( atCat-> left!= nullptr && atCat->right == nullptr ){
      cout << atCat->name << " begat " << atCat->left->name << endl;
   }
   else if( atCat->left == nullptr && atCat->right != nullptr ){
      cout << atCat->name << " begat " << atCat->right->name << endl;
   }
   dfsPreorder( atCat->left );
   dfsPreorder( atCat->right );
}

void CatEmpire::getEnglishSuffix (int i) const {
   if( i <= 0 ) cout << "Invalid" << endl;
   if( (i % 100 >= 10) && (i % 100 <= 20) ){
      cout << "th";
   }
   else if ( i % 10 == 1 )
      cout << "st";
   else if ( i % 10 == 2 )
      cout << "nd";
   else if ( i % 10 == 3 )
      cout << "rd";
   else 
      cout << "th";
}





void CatEmpire::catGenerations() const {
   int gen = 1;
   queue<Cat*> catQueue;
   catQueue.push( topCat );
   catQueue.push( nullptr );
   cout << gen;
   getEnglishSuffix(gen);
   cout << " Generation" << endl;
   cout << "  ";
   while ( !catQueue.empty() ){
      Cat* cat = catQueue.front(); 
      catQueue.pop();
      if( cat == nullptr ){
         gen=gen+1;
         catQueue.push(nullptr);
         if(catQueue.front() == nullptr) break;
         else{
         cout << endl;
         cout << gen;
         getEnglishSuffix(gen);
         cout << " Generation" << endl;
         cout << "  " ;
         continue;
         }
      }

      if(cat->left!=nullptr){
         catQueue.push(cat->left);
      }
      if(cat->right!=nullptr){
         catQueue.push(cat->right);
      }
         cout << cat->name << "  ";
   
   }
         cout << endl;
}



